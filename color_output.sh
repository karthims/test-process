# For loop with a condition check
for (( i=1; i<=5; i++ ))
do
    if [ $i -le 3 ]; then
        echo "Iteration $i: Condition is true"
    else
        echo "Iteration $i: Condition is false"
    fi
done
