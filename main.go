// main.go
package main

import "fmt"

func Add(a, b int) int {
	return a + b
}

func Multiply(a, b int) int {
	return a * b
}

func Subtract(a, b int) int {
	return a - b
}

func main() {
	resultAdd := Add(2, 3)
	resultMultiply := Multiply(4, 5)
	resultSubtract := Subtract(10, 7)

	fmt.Printf("Addition result: %d\n", resultAdd)
	fmt.Printf("Multiplication result: %d\n", resultMultiply)
	fmt.Printf("Subtraction result: %d\n", resultSubtract)
}

