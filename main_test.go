// main_test.go
package main

import "testing"

func TestAdd(t *testing.T) {
	result := Add(2, 3)
	expected := 5

	if result != expected {
		t.Errorf("Add(2, 3) returned %d, expected %d", result, expected)
	}
}

func TestMultiply(t *testing.T) {
	result := Multiply(4, 5)
	expected := 20

	if result != expected {
		t.Errorf("Multiply(4, 5) returned %d, expected %d", result, expected)
	}
}

func TestSubtract(t *testing.T) {
	result := Subtract(10, 6)
	expected := 4

	if result != expected {
		t.Errorf("Subtract(10, 7) returned %d, expected %d", result, expected)
	}
}
